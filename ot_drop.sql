--------------------------------------------------------------------------------------
-- Name	       : OT (Oracle Tutorial) Sample Database
-- Link	       : http://www.oracletutorial.com/oracle-sample-database/
-- Version     : 1.0
-- Last Updated: July-28-2017
-- Copyright   : Copyright � 2017 by www.oracletutorial.com. All Rights Reserved.
-- Notice      : Use this sample database for the educational purpose only.
--               Credit the site oracletutorial.com explitly in your materials that
--               use this sample database.
--------------------------------------------------------------------------------------
DROP TABLE order_items PURGE;  

DROP TABLE orders PURGE;
drop sequence "ORDERS_SEQUENCE";

DROP TABLE inventories PURGE;

DROP TABLE products PURGE;
drop sequence "PRODUCTS_SEQUENCE";

DROP TABLE product_categories PURGE;
drop sequence "PRODUCT_CATEGORIES_SEQUENCE";

DROP TABLE warehouses PURGE;
drop sequence "WAREHOUSES_SEQUENCE";

DROP TABLE employees PURGE;
drop sequence "EMPLOYEES_SEQUENCE";

DROP TABLE contacts PURGE;
drop sequence "CONTACTS_SEQUENCE";

DROP TABLE customers PURGE;
drop sequence "CUSTOMERS_SEQUENCE";

DROP TABLE locations PURGE;
drop sequence "LOCATIONS_SEQUENCE";

DROP TABLE countries PURGE;

DROP TABLE regions PURGE;
DROP SEQUENCE "REGIONS_SEQUENCE";