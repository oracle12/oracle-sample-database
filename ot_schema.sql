--------------------------------------------------------------------------------------
-- Name	       : OT (Oracle Tutorial) Sample Database
-- Link	       : http://www.oracletutorial.com/oracle-sample-database/
-- Version     : 1.0
-- Last Updated: July-28-2017
-- Copyright   : Copyright � 2017 by www.oracletutorial.com. All Rights Reserved.
-- Notice      : Use this sample database for the educational purpose only.
--               Credit the site oracletutorial.com explitly in your materials that
--               use this sample database.
--------------------------------------------------------------------------------------


---------------------------------------------------------------------------
-- execute the following statements to create tables
---------------------------------------------------------------------------
-- regions
CREATE TABLE regions(
    region_id NUMBER PRIMARY KEY,
    region_name VARCHAR2( 50 ) NOT NULL
  );
  
    CREATE SEQUENCE regions_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER regions_trigger 
        BEFORE INSERT ON regions 
        FOR EACH ROW
        
        BEGIN
          SELECT regions_sequence.NEXTVAL
          INTO   :new.region_id
          FROM   dual;
        END;
        
/

-- countries table
CREATE TABLE countries
  (
    country_id   CHAR( 2 ) PRIMARY KEY  ,
    country_name VARCHAR2( 40 ) NOT NULL,
    region_id    NUMBER                 , -- fk
    CONSTRAINT fk_countries_regions FOREIGN KEY( region_id )
      REFERENCES regions( region_id ) 
      ON DELETE CASCADE
  );

-- location
CREATE TABLE locations
  (
    location_id NUMBER PRIMARY KEY      ,
    address     VARCHAR2( 255 ) NOT NULL,
    postal_code VARCHAR2( 20 )          ,
    city        VARCHAR2( 50 )          ,
    state       VARCHAR2( 50 )          ,
    country_id  CHAR( 2 )               , -- fk
    CONSTRAINT fk_locations_countries 
      FOREIGN KEY( country_id )
      REFERENCES countries( country_id ) 
      ON DELETE CASCADE
  );
  
    CREATE SEQUENCE locations_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER locations_trigger 
        BEFORE INSERT ON locations 
        FOR EACH ROW
        
        BEGIN
          SELECT locations_sequence.NEXTVAL
          INTO   :new.location_id
          FROM   dual;
        END;
        
/
  
-- warehouses
CREATE TABLE warehouses
  (
    warehouse_id NUMBER PRIMARY KEY,
    warehouse_name VARCHAR( 255 ) ,
    location_id    NUMBER( 12, 0 ), -- fk
    CONSTRAINT fk_warehouses_locations 
      FOREIGN KEY( location_id )
      REFERENCES locations( location_id ) 
      ON DELETE CASCADE
  );
  

    CREATE SEQUENCE warehouses_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER warehouses_trigger 
        BEFORE INSERT ON warehouses 
        FOR EACH ROW
        
        BEGIN
          SELECT warehouses_sequence.NEXTVAL
          INTO   :new.warehouse_id
          FROM   dual;
        END;
        
/

  
-- employees
CREATE TABLE employees
  (
    employee_id NUMBER PRIMARY KEY    ,
    first_name VARCHAR( 255 ) NOT NULL,
    last_name  VARCHAR( 255 ) NOT NULL,
    email      VARCHAR( 255 ) NOT NULL,
    phone      VARCHAR( 50 ) NOT NULL ,
    hire_date  DATE NOT NULL          ,
    manager_id NUMBER( 12, 0 )        , -- fk
    job_title  VARCHAR( 255 ) NOT NULL
--    ,
--    CONSTRAINT fk_employees_manager 
--        FOREIGN KEY( manager_id )
--        REFERENCES employees( employee_id )
--        ON DELETE CASCADE
  );
  
  
    CREATE SEQUENCE employees_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER employees_trigger 
        BEFORE INSERT ON employees 
        FOR EACH ROW
        
        BEGIN
          SELECT employees_sequence.NEXTVAL
          INTO   :new.employee_id
          FROM   dual;
        END;
        
/  
  
  
-- product category
CREATE TABLE product_categories
  (
    category_id NUMBER PRIMARY KEY          ,
    category_name VARCHAR2( 255 ) NOT NULL
  );
  
    CREATE SEQUENCE product_categories_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER product_categories_trigger 
        BEFORE INSERT ON product_categories 
        FOR EACH ROW
        
        BEGIN
          SELECT product_categories_sequence.NEXTVAL
          INTO   :new.category_id
          FROM   dual;
        END;
        
/    

-- products table
CREATE TABLE products
  (
    product_id NUMBER PRIMARY KEY         ,
    product_name  VARCHAR2( 255 ) NOT NULL,
    description   VARCHAR2( 2000 )        ,
    standard_cost NUMBER( 9, 2 )          ,
    list_price    NUMBER( 9, 2 )          ,
    category_id   NUMBER NOT NULL         ,
    CONSTRAINT fk_products_categories 
      FOREIGN KEY( category_id )
      REFERENCES product_categories( category_id ) 
      ON DELETE CASCADE
  );
  
    CREATE SEQUENCE products_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER products_trigger 
        BEFORE INSERT ON products 
        FOR EACH ROW
        
        BEGIN
          SELECT products_sequence.NEXTVAL
          INTO   :new.product_id
          FROM   dual;
        END;
        
/
  
  
-- customers
CREATE TABLE customers
  (
    customer_id NUMBER PRIMARY KEY,
    name         VARCHAR2( 255 ) NOT NULL,
    address      VARCHAR2( 255 )         ,
    website      VARCHAR2( 255 )         ,
    credit_limit NUMBER( 8, 2 )
  );
  
    CREATE SEQUENCE customers_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER customers_trigger 
        BEFORE INSERT ON customers 
        FOR EACH ROW
        
        BEGIN
          SELECT customers_sequence.NEXTVAL
          INTO   :new.customer_id
          FROM   dual;
        END;
        
/
  
-- contacts
CREATE TABLE contacts
  (
    contact_id NUMBER PRIMARY KEY       ,
    first_name  VARCHAR2( 255 ) NOT NULL,
    last_name   VARCHAR2( 255 ) NOT NULL,
    email       VARCHAR2( 255 ) NOT NULL,
    phone       VARCHAR2( 20 )          ,
    customer_id NUMBER                  ,
    CONSTRAINT fk_contacts_customers 
      FOREIGN KEY( customer_id )
      REFERENCES customers( customer_id ) 
      ON DELETE CASCADE
  );
  
      CREATE SEQUENCE contacts_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER contacts_trigger 
        BEFORE INSERT ON contacts 
        FOR EACH ROW
        
        BEGIN
          SELECT contacts_sequence.NEXTVAL
          INTO   :new.contact_id
          FROM   dual;
        END;
        
/
  
  
-- orders table
CREATE TABLE orders
  (
    order_id NUMBER PRIMARY KEY        ,
    customer_id NUMBER( 6, 0 ) NOT NULL, -- fk
    status      VARCHAR( 20 ) NOT NULL ,
    salesman_id NUMBER( 6, 0 )         , -- fk
    order_date  DATE NOT NULL          ,
    CONSTRAINT fk_orders_customers 
      FOREIGN KEY( customer_id )
      REFERENCES customers( customer_id )
      ON DELETE CASCADE,
    CONSTRAINT fk_orders_employees 
      FOREIGN KEY( salesman_id )
      REFERENCES employees( employee_id ) 
      ON DELETE SET NULL
  );
  
  
    CREATE SEQUENCE orders_sequence
      START WITH 1
      INCREMENT BY 1;
      
    CREATE OR REPLACE TRIGGER orders_trigger 
        BEFORE INSERT ON orders 
        FOR EACH ROW
        
        BEGIN
          SELECT orders_sequence.NEXTVAL
          INTO   :new.order_id
          FROM   dual;
        END;
        
/  
  
  
-- order items
CREATE TABLE order_items
  (
    order_id   NUMBER( 12, 0 )                                , -- fk
    item_id    NUMBER( 12, 0 )                                ,
    product_id NUMBER( 12, 0 ) NOT NULL                       , -- fk
    quantity   NUMBER( 8, 2 ) NOT NULL                        ,
    unit_price NUMBER( 8, 2 ) NOT NULL                        ,
    CONSTRAINT pk_order_items 
      PRIMARY KEY( order_id, item_id ),
    CONSTRAINT fk_order_items_products 
      FOREIGN KEY( product_id )
      REFERENCES products( product_id ) 
      ON DELETE CASCADE,
    CONSTRAINT fk_order_items_orders 
      FOREIGN KEY( order_id )
      REFERENCES orders( order_id ) 
      ON DELETE CASCADE
  );
  
  
-- inventories
CREATE TABLE inventories
  (
    product_id   NUMBER( 12, 0 )        , -- fk
    warehouse_id NUMBER( 12, 0 )        , -- fk
    quantity     NUMBER( 8, 0 ) NOT NULL,
    CONSTRAINT pk_inventories 
      PRIMARY KEY( product_id, warehouse_id ),
    CONSTRAINT fk_inventories_products 
      FOREIGN KEY( product_id )
      REFERENCES products( product_id ) 
      ON DELETE CASCADE,
    CONSTRAINT fk_inventories_warehouses 
      FOREIGN KEY( warehouse_id )
      REFERENCES warehouses( warehouse_id ) 
      ON DELETE CASCADE
  );